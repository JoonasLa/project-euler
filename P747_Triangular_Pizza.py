from numba import njit
import numpy as np

def euler0():  # [(1, 17157287), (2, 10102051), (3, 7179677), (4, 5572809), (5, 4554885), (6, 3851860), (7, 3337045), (8, 2943725), (9, 2633404), (10, 2382304), (11, 2174941), (12, 2000801), (13, 1852487), (14, 1724651), (15, 1613323), (16, 1515499), (17, 1428863), (18, 1351598), (19, 1282262), (20, 1219694), (21, 1162948), (22, 1111248), (23, 1063950), (24, 1020514), (25, 980486), (26, 943480), (27, 909166), (28, 877260), (29, 847518), (30, 819727), (31, 793701), (32, 769276), (33, 746310), (34, 724676), (35, 704260), (36, 684964), (37, 666696), (38, 649378), (39, 632937), (40, 617307), (41, 602431), (42, 588256), (43, 574732), (44, 561815), (45, 549467), (46, 537650), (47, 526330), (48, 515478), (49, 505063), (50, 495062), (51, 485448), (52, 476201), (53, 467300), (54, 458725), (55, 450460), (56, 442487), (57, 434791), (58, 427358), (59, 420175), (60, 413230), (61, 406511), (62, 400006), (63, 393707), (64, 387603), (65, 381685), (66, 375945), (67, 370375), (68, 364968), (69, 359717), (70, 354614), (7... truncated
    n = 100000000
    m2 = 2
    m3 = 1
    a = -n*m2-n
    b = n + m2+ m3+ 1
    c = -1 - m3
    D = b**2 - 4 *a * c
    sD = np.sqrt(D)
    print((-b + sD) / (2*a))


def euler9():
    m2 = 4999
    m3 = 4999
    a = 1
    b = (-2*m2-2*m3-4*m2*m3-2)
    c = (m2**2+m3**2+2*m2*m3+2*m2+2*m3+1)
    D = b**2-4*a*c
    sD = np.sqrt(D)
    n2 = (-b + sD)/(2*a)
    print(a, b, c, D, (-b + sD)/(2*a), (-b - sD) / (2*a))
    print(np.ceil(n2) == n2)

def euler0():
    lista = {0: [(0, 0, 0)]}
    for m2 in range(1, 5000):
        m3 = m2
        a = 1
        b = (-2 * m2 - 2 * m3 - 4 * m2 * m3 - 2)
        c = (m2 ** 2 + m3 ** 2 + 2 * m2 * m3 + 2 * m2 + 2 * m3 + 1)
        D = b ** 2 - 4 * a * c
        sD = np.sqrt(D)
        n2 = int((-b + sD) / (2 * a))
        for j in range(-1, 2):
            n3 = n2 + j
            if a*n3**2 + b*n3 + c == 0:
                break
        else:
            print("Virhe")
        lista[n3] = [(0, 0, 3)]
        n3 += 1
        lista[n3] = [(m2+1, m2, 3)]

    euler_numba(lista)


@njit()
def euler_numba(lista: dict):
    print(475327908531033277 % 1000000007)
    total = 0
    lisa = 0
    N = 100000000

    # for n in sorted(list(lista.keys())):
        # print(f"{n:5d} {lista[n]}")

    for n in range(3, N+1):
        total += (n-1)*(n-2)//2
        total += (n-2)*6

        if n in lista:
            for m2, m3, l in lista.pop(n):
                lisa += l
                if m2 == 0:
                    continue
                a = 1
                b = (-2 * m2 - 2 * m3 - 4 * m2 * m3 - 2)
                c = (m2 ** 2 + m3 ** 2 + 2 * m2 * m3 + 2 * m2 + 2 * m3 + 1)
                D = b ** 2 - 4 * a * c
                sD = np.sqrt(D)
                n2 = int((-b + sD) / (2*a))
                for j in range(-1, 2):
                    n3 = n2 + j
                    tot = a*n3**2+b*n3+c
                    if tot == 0:
                        if n3 not in lista:
                            lista[n3] = list()
                        lista[n3].append((0, 0, 6))
                        n3 += 1
                        if n3 not in lista:
                            lista[n3] = list()
                        lista[n3].append((m2+1, m3, 6))
                        break
                    if tot > 0:
                        if n3 not in lista:
                            lista[n3] = list()
                        lista[n3].append((m2+1, m3, 12))
                        break

        if n % 100000 == 0:
            print(n, lisa)
        total += lisa
    # for n in sorted(list(lista.keys())):
        # print(f"{n:5d} {lista[n]}")
    # print(lista[-1])
    print(N, total, total % 1000000007)

# @njit()
def euler():
    total = 0
    N = 100000000

    for n in range(3, N+1):
        total += (n-1)*(n-2)//2
        total += (n-2)*6
    print("Helpot laskettu", total)
    total1 = total
    total2 = euler_numba(N)
    total += total2
    print("Valmis")
    print(N, total, total % 1000000007)
    print("Alunperin", total1, "Lisäyksiä", total - total1)

@njit()
def euler_numba(N):
    total = 0
    m3 = 1
    m2 = 2
    while m2 != m3:
        print("m3", m3)
        m2 = m3
        while True:
            a = 1
            b = (-2 * m2 - 2 * m3 - 4 * m2 * m3 - 2)
            c = (m2 ** 2 + m3 ** 2 + 2 * m2 * m3 + 2 * m2 + 2 * m3 + 1)
            D = b ** 2 - 4 * a * c
            sD = np.sqrt(D)
            n2 = int((-b + sD) / (2*a))

            for j in range(-1, 2):

                n3 = n2 + j
                if n3 > N:
                    break
                tot = a*n3**2+b*n3+c
                if m2 == m3:
                    if tot == 0:
                        total += 3 + (N-n3) * 6
                        break
                    if tot > 0:
                        total += (N-n3 + 1) * 6
                        break
                else:
                    if tot == 0:
                        total += 6 + (N-n3) * 12
                        break
                    if tot > 0:
                        total += (N-n3 + 1) * 12
                        break

            if n3 > N:
                break
            m2 += 1
            if m3 > 4900:
                print("m3, m2", m3, m2)
        if m2 != m3:
            m3 += 1
    return total




# 100000000 475327908531033277


if __name__ == "__main__":
    euler()