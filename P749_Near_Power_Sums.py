import numpy as np
from numba import njit, uint64


def main():
    tot = []
    for s in range(2, 17):
        lista = nps2(s)
        print(s, lista)
        tot += lista
    summa = 0
    for k in tot:
        summa += k
    print("Yhteensä", summa)


@njit()
def nps1(S):
    num = np.zeros(S, dtype=uint64)
    num[0] = 1
    # print(num)
    luku = 10**(S-1)
    i = S - 1
    vast = [0]
    while i > -1:
        k = 1
        edtot = None
        while True:
            tot = np.sum(num**k)
            # print(luku, num, k, tot)
            if tot == luku - 1 or tot == luku + 1:
                vast.append(luku)
                break
            if tot > luku + 1:
                break
            k += 1
            if edtot is not None and tot == edtot:
                break
            edtot = tot
        i = S - 1
        while i > -1:
            num[i] += 1
            luku += 10 ** (S-i-1)
            if num[i] == 10:
                luku -= 10 ** (S-i)
                num[i] = 0
                i -= 1
                continue
            break

    return vast


# @njit()
def nps2(S):
    num = np.zeros(S, dtype=int)
    num[-1] = 1
    i = S - 1
    vast = [0]
    r1, r2 = 10 ** (S-1), 10 ** S
    while True:
        i = S - 1
        num[i] += 1
        while i >= 0 and num[i] == 10:
            i -= 1
            num[i] += 1
        if i < 0:
            break
        for i2 in range(i+1, S):
            num[i2] = num[i2-1]
        numsum = np.zeros(10, dtype=int)
        for i in range(S):
            numsum[num[i]] += 1
        # print(num, numsum)
        k = 1
        toted = 0
        while True:
            tot = np.sum(num ** k)
            k += 1
            if tot >= r2 or tot == toted:
                break
            toted = tot
            if r1 <= tot:
                tot2, tot3 = tot - 1, tot + 1
                nsum2 = np.zeros(10, dtype=int)
                mahd = True
                if tot2 >= r1:
                    while tot2 > 0:
                        tot2, n = tot2 // 10, tot2 % 10
                        nsum2[n] += 1
                        if nsum2[n] > numsum[n]:
                            mahd = False
                            break
                    if mahd:
                        vast.append(tot-1)
                if tot3 < r2:
                    nsum3 = np.zeros(10, dtype=int)
                    mahd = True
                    while tot3 > 0:
                        tot3, n = tot3 // 10, tot3 % 10
                        nsum3[n] += 1
                        if nsum3[n] > numsum[n]:
                            mahd = False
                            break
                    if mahd:
                        vast.append(tot+1)


    return vast


if __name__ == "__main__":
    main()
